<?php
$dotenv = Dotenv\Dotenv::createMutable(__DIR__.'/../');
$dotenv->load();
$dbname = $_ENV['DB_DATABASE'];
$servername = $_ENV['APP_URL'];
$username = $_ENV['DB_USERNAME'];
$password = $_ENV['DB_PASSWORD'];
$link = mysqli_connect($servername, $username, $password);

// Check connection
if ($link === false) {
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

$sql = "CREATE DATABASE IF NOT EXISTS drugstore character set UTF8";

if (mysqli_query($link, $sql)) {
    echo "Database created successfully";
    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    else{
        echo "Connect database successful";
        // sql to create table
        $sql = "CREATE TABLE medicine (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(100) NOT NULL,
        declaration_date VARCHAR(30) NOT NULL,
        ingredient TEXT,
        declare_unit VARCHAR(255),
        label VARCHAR(50),
        dosage_form VARCHAR(100),
        concentration VARCHAR(100),
        packing VARCHAR(50),
        unit VARCHAR(20),
        classify VARCHAR(100),
        status VARCHAR(20),
        price DOUBLE,
        facilities VARCHAR(50),
        country VARCHAR(50)
        )";

        if ($conn->query($sql) === TRUE) {
            echo "Table MyGuests created successfully";
        } else {
            echo "Error creating table: " . $conn->error;
        }
    }
} else {
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}

// Close connection
mysqli_close($link);
?>